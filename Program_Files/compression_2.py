import gzip
import shutil
import time
start = time.time()
with open('IMUdata.csv', 'rb') as f_in:
	with open('IMUdata.csv.gz', 'wb') as f_out:
		with gzip.GzipFile('IMUdata.csv', 'wb', fileobj=f_out) as f_out:
			shutil.copyfileobj(f_in,f_out)
end = time.time()
print("Runtime of the program is", { end - start}," seconds.")
