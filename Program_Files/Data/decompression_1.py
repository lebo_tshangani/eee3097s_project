import zlib
import csv

import time
start = time.time()

compressed_data = open('file.csv','rb').read()
decompressed_data = zlib.decompress(compressed_data)

f=open('data.csv', 'wb')
f.write(decompressed_data)
f.close()

end = time.time()
print("Runtime of the program is", { end - start}," seconds.")

